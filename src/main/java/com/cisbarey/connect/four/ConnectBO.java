package com.cisbarey.connect.four;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectBO {
	
	@Autowired private BoardMongoRepository boardMongoRepository;
	
	public static final Integer ROWS = 6;
	public static final Integer COLUMNS = 7;
	
	public Response init() {
		Board board = new Board();
		board.setDiscs(empty());
		board.setPlayer(1);
		board.setWin(Boolean.FALSE);
		
		return new Response(boardMongoRepository.save(board));
	}
	
	public Response reload(String id) {
		Board board = boardMongoRepository.findOne(id);
		
		if (board != null) {
			board.setPlayer(1);
			board.setWin(Boolean.FALSE);
			board.setDiscs(empty());
			return new Response(boardMongoRepository.save(board));
		} else {
			return new Response("Usuario inválido.");
		}
	}
	
	public Response play(String id, int player, int column) {
		Board board = boardMongoRepository.findOne(id);
		
		if (board != null) {
			if (!board.isWin()) {
				Disc[][] discs = board.getDiscs();
				if (isFullBoard(discs)) {
					return new Response("El tablero está lleno, empate, vuelve a cargar el tablero.");
				} else {
					if (player == board.getPlayer()) {
						int lengthI = discs.length - 1;
						
						for(int i = lengthI; i >= 0; i--) {
							if (discs[i][column] == Disc.NONE) {
								discs[i][column] = player == 1 ? Disc.RED : Disc.YELLOW;
								board.setDiscs(discs);
								board.setPlayer(player == 1 ? 2 : 1);
								Response response = null;
								Disc disc = boardValidate(discs);
								switch (disc) {
								case RED:
									board.setWin(Boolean.TRUE);
									response = new Response(boardMongoRepository.save(board));
									response.setMessage("El ganador es el Jugador 1");
									return response;
								case YELLOW:
									board.setWin(Boolean.TRUE);
									response = new Response(boardMongoRepository.save(board));
									response.setMessage("El ganador es el Jugador 2");
									return response;
								default:
									board.setWin(Boolean.FALSE);
									return new Response(boardMongoRepository.save(board));
								}
							}
						}
						return new Response("Columna llena, intenta en otra columna.");
					} else {
						return new Response("Es el turno del jugador " + (player == 1 ? 2 : 1) + ".");
					}
				}
			} else {
				Response response = new Response(board);
				response.setMessage("El ganador es el Jugador " + (board.getPlayer() == 1 ? 2 : 1));
				return response;
			}
		} else {
			return new Response("Usuario inválido.");
		}
	}
	
	private Disc[][] empty() {
		Disc[][] discs = new Disc[ROWS][COLUMNS];
		for(int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLUMNS; j++) {
				discs[i][j] = Disc.NONE;
			}
		}
		return discs;
	}
	
	private Disc boardValidate(Disc[][] discs) {
		
		Disc disc = winInHorizontal(discs);
		if (disc != Disc.NONE) {
			return disc;
		}
		disc = winInVertical(discs);
		if (disc != Disc.NONE) {
			return disc;
		}
		disc = winInDiagonal(discs);
		if (disc != Disc.NONE) {
			return disc;
		}
		return disc;
	}
	
	private Disc winInHorizontal(Disc[][] discs) {
		for (int i = ROWS - 1; i >= 0; i--) {
			int red = 0;
			int yellow = 0;
			for (int j = 0; j < COLUMNS; j++) {
				switch (discs[i][j]) {
				case RED:
					red += 1;
					yellow = 0;
					break;
				case YELLOW:
					yellow += 1;
					red = 0;
					break;
				default:
					break;
				}
				
				if (red >= 4) {
					return Disc.RED;
				}
				if (yellow >= 4) {
					return Disc.YELLOW;
				}
			}
		}
		return Disc.NONE;
	}
	
	private Disc winInVertical(Disc[][] discs) {
		for (int j = COLUMNS - 1; j >= 0; j--) {
			int red = 0;
			int yellow = 0;
			for (int i = ROWS - 1; i >= 0; i--) {
				switch (discs[i][j]) {
				case RED:
					red += 1;
					yellow = 0;
					break;
				case YELLOW:
					yellow += 1;
					red = 0;
					break;
				default:
					break;
				}
				
				if (red >= 4) {
					return Disc.RED;
				}
				if (yellow >= 4) {
					return Disc.YELLOW;
				}
			}
		}
		return Disc.NONE;
	}
	
	private Disc winInDiagonal(Disc[][] discs) {
		int iMax = ROWS - 1; //5
		
		for (int i = iMax; i >= 0; i--) {
			boolean up = Boolean.TRUE;
			if (i < 3) {
				up = Boolean.FALSE;
			}
			int jMax = COLUMNS - 1; //6
			for (int j = jMax; j >= 3; j--) {
				switch (discs[i][j]) {
				case RED:
					if (diagonalValidUp(discs, i, j, Disc.RED, up)) return Disc.RED;
					break;
				case YELLOW:
					if (diagonalValidUp(discs, i, j, Disc.YELLOW, up)) return Disc.YELLOW;
					break;
				default:
					break;
				}
			}
		}
		return Disc.NONE;
	}
	
	private boolean diagonalValidUp(Disc[][] discs, int i, int j, Disc disc, boolean up) {
		for (int k = 0; k < 4; k++) {
			if (discs[up ? i-- : i++][j--] != disc) {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}
	
	private boolean isFullBoard(Disc[][] discs) {
		int lengthRows = discs.length - 1;
		for (int i = lengthRows; i >= 0; i--) {
			int lengthColumns = discs[i].length - 1;
			for (int j = lengthColumns; j >= 0; j--) {
				if (discs[i][j] == Disc.NONE) {
					return Boolean.FALSE;
				}
			}
		}
		return Boolean.TRUE;
	}

}
