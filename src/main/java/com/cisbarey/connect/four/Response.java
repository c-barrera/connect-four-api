package com.cisbarey.connect.four;

public class Response {
	
	private boolean error = Boolean.FALSE;
	private String message;
	private Board board;
	
	public Response(String message) {
		this.error = Boolean.TRUE;
		this.message = message;
	}
	
	public Response(Board board) {
		this.board = board;
	}

	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
}
