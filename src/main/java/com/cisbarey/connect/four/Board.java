package com.cisbarey.connect.four;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "board")
public class Board {
	
	@Id
	private String id;
	private int player;
	private boolean win;
	private Disc[][] discs;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getPlayer() {
		return player;
	}
	public void setPlayer(int player) {
		this.player = player;
	}
	public boolean isWin() {
		return win;
	}
	public void setWin(boolean win) {
		this.win = win;
	}
	public Disc[][] getDiscs() {
		return discs;
	}
	public void setDiscs(Disc[][] discs) {
		this.discs = discs;
	}
}
