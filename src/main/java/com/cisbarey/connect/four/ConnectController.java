package com.cisbarey.connect.four;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/connect4")
public class ConnectController {
	
	@Autowired private ConnectBO connectBO;
	
	@RequestMapping
    public Response init() {
        return connectBO.init();
    }
	
	@RequestMapping("/{id}/play/reload")
	public Response reload(@PathVariable String id) {
		return connectBO.reload(id);
	}
	
	@RequestMapping("/{id}/play/{player}/column/{column}")
	public Response play(
			@PathVariable String id, 
			@PathVariable int player,
			@PathVariable int column
			) {
		return connectBO.play(id, player, column);
	}
}
